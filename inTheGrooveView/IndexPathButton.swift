//
//  IndexPathButton.swift
//  inTheGrooveView
//
//  Created by Rishil on 4/26/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit

class IndexPathButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    var indexPath : IndexPath = IndexPath(row: 0, section: 0)
    
    
    func getPath() -> IndexPath {
        return indexPath
    }
    
    func setPath(path: IndexPath) {
        self.indexPath = path
    }
}

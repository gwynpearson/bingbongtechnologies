//
//  PlayButton.swift
//  inTheGrooveView
//
//  Created by Gwyneth Pearson on 4/25/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit

class RoutineButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var routine : Routine = Routine(name:"",tasks:[])
    
    
    func getRoutine() -> Routine {
        return routine
    }
    
    func setRoutine(routine: Routine) {
        self.routine = routine
    }

}

//
//  Section.swift
//  inTheGrooveView
//
//  Created by Rishil on 4/12/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import Foundation

struct Section {
    
    var routine : Routine
    var collapsed : Bool
    
    init(routine: Routine, collapsed : Bool = true) {
        self.routine = routine
        self.collapsed = collapsed
    }
    
    
    
}

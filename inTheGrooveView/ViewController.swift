//
//  ViewController.swift
//  inTheGrooveView
//
//  Created by zach greenberg on 3/22/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    var testTable: UITableView!
    
    private func setupTableView() {
        
        testTable = UITableView(frame: view.frame.offsetBy(dx: 0, dy: 200))
        testTable.dataSource = self
        testTable.delegate = self
        testTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
       view.addSubview(testTable)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        cell.textLabel?.text = " hello"
        print(cell.textLabel?.text)
        return cell
    }
    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//    }

     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {}
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupTableView()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


//
//  EditRoutineViewController.swift
//  inTheGrooveView
//
//  Created by zach greenberg on 4/6/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit

class EditRoutineViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var task: [Task] = [Task(name: "brushing", time: 120, songs: nil)]
    var routine: Routine!

    @IBOutlet weak var routineTable: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return task.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell")as! TaskCell
        cell.taskName.text = task[indexPath.row].name
        cell.timeVal.text = String(task[indexPath.row].time)
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        routineTable.delegate = self
        routineTable.dataSource = self
        // Do any additional setup after loading the view.
        routine = Routine(name: "sample", tasks: task)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

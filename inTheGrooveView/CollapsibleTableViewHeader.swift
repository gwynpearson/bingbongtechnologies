//
//  CollapsibleTableViewHeader.swift
//  inTheGrooveView
//
//  Created by Rishil on 4/12/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import Foundation
import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(header: CollapsibleTableViewHeader, section: Int)
}

class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
   // let titleText = UITextField()
    
    
    let titleLabel = UITextField()
    let arrowLabel = UILabel()
    let playButton = RoutineButton(type: UIButtonType.custom)
    let editRButton = IndexPathButton(type: UIButtonType.custom)
    //let addTask = UIButton(type: UIButtonType.custom)

    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.addSubview(editRButton)
       // contentView.addSubview(titleText)
       contentView.addSubview(titleLabel)
       contentView.addSubview(arrowLabel)
        contentView.addSubview(playButton)
     //   contentView.addSubview(addTask)
        
        //Set constraints for arrow
       // titleLabel.widthAnchor.constraint(equalToConstant:12).isActive = true
        arrowLabel.widthAnchor.constraint(equalToConstant: 20).isActive = true
        arrowLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
       // titleText.widthAnchor.constraint(equalToConstant: 12).isActive = true
       // titleText.heightAnchor.constraint(equalToConstant: 12).isActive = true
        
       //titleLabel.translatesAutoresizingMaskIntoConstraints = false
        arrowLabel.translatesAutoresizingMaskIntoConstraints = false
        playButton.translatesAutoresizingMaskIntoConstraints = false
        editRButton.translatesAutoresizingMaskIntoConstraints = false

     //   titleText.translatesAutoresizingMaskIntoConstraints = false
        playButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        playButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        editRButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        editRButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        titleLabel.isEnabled = true
        //titleText.isHidden = true
//        addTask.translatesAutoresizingMaskIntoConstraints = false
//        addTask.widthAnchor.constraint(equalToConstant: 20).isActive = true
//        addTask.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader)))
        
    }
    
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
            return
        }
        delegate?.toggleSection(header: self, section: cell.section)
    }
    
    func setCollapsed(collapsed: Bool) {
        //Animate arrow rotation
        arrowLabel.rotate(collapsed ? 0.0 : CGFloat(M_PI_2))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let views = [
            "titleLabel" : titleLabel,
            "arrowLabel" : arrowLabel,
            "playButton" : playButton,
            //"titleText"  : titleText,
            "editRButton" : editRButton
        //    "addTask"    : addTask
        
        ]
        
      contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "H:|-175-[titleLabel]-|", options: [], metrics: nil, views: views).first!) //not what we saw in the tutorial... this might cause a bug (null exception)
        //contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "H:|-175-[titleText]-|", options: [], metrics: nil, views: views).first!)
       contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "H:|-185-[arrowLabel]-|", options: [], metrics: nil, views: views).first!)
//         contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "H:|-300-[addTask]-|", options: [], metrics: nil, views: views).first!)
         contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "H:|-210-[editRButton]-|", options: [], metrics: nil, views: views).first!)
        contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "H:|-265-[playButton]-|", options: [], metrics: nil, views: views).first!)
    contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[titleLabel]-|", options: [], metrics: nil, views: views).first!) //not what we saw in the tutorial... this might cause a bug (null exception)
        // contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[titleText]-|", options: [], metrics: nil, views: views).first!)
       contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[arrowLabel]-|", options: [], metrics: nil, views: views).first!) //not what we saw in the tutorial... this might cause a bug (null exception)
//        contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[addTask]-|", options: [], metrics: nil, views: views).first!) //not what we saw in the tutorial... this might cause a bug (null exception)
      contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[playButton]-|", options: [], metrics: nil, views: views).first!)
         contentView.addConstraint(NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[editRButton]-|", options: [], metrics: nil, views: views).first!)
    }
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

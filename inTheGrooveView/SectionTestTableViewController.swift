//
//  SectionTestTableViewController.swift
//  inTheGrooveView
//
//  Created by Rishil on 4/10/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit
import MediaPlayer

class SectionTestTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBAction func addNewTaskClicked(_ sender: Any) {
         performSegue(withIdentifier: "newTaskSegue", sender: sender)
    }

//    var taskOne : Task!
//    var taskTwo : Task!
//    var taskThree : Task!
//    
//    var taskArrayOne : [Task]!
//    var taskArrayTwo : [Task]!
//    var routineOne : Routine!
//    var routineTwo: Routine!
//    
//    var sectionOne : Section!
//    var sectionTwo : Section!
    
    var routineArray : [Routine]!
    
    var sectionArray : [Section]!
    var count = 0
    
    var songs : [MPMediaItem]? = MPMediaQuery.songs().items
    
    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {}
    @IBAction func unwindFromEdit(segue: UIStoryboardSegue) {}

    
    @IBOutlet weak var theTableView: UITableView!
    @IBAction func addRoutine(_ sender: UIButton) {
      
        print("adding routines")
        count += 1
        let (songsOne, indexSetOne) = generateTask(taskTime: Double(120), routineIndexSet : Set<Int>())
        let taskOne = Task(name: "Brushing", time: 120, songs: songsOne)
        taskOne.indexSet = indexSetOne
        
        let (songsTwo, indexSetTwo) = generateTask(taskTime: Double(300), routineIndexSet : indexSetOne)
        let taskTwo = Task(name: "Showering", time: 300, songs: songsTwo)
        taskTwo.indexSet = indexSetTwo
        let taskArray = [taskOne, taskTwo]
        let routineToAdd = Routine(name: "Sample Routine \(count)", tasks: taskArray)
        routineArray = loadRoutine()
        routineArray.append(routineToAdd)
        sectionArray.append(Section(routine: routineToAdd)) //may not be necessary...
        for routine in routineArray{
            print("routine name\(routine.name)")
        }
        saveRoutine(routineArray: routineArray)
        print("count is \(count)")
        theTableView.reloadData()
    }
    
    func getSectionsArrayFromRoutine( from routineArray : [Routine]) -> [Section]{
        print("adding routine sections")
        var i = 0;
        sectionArray = []
        for routine in routineArray{
            i += 1
            sectionArray.append(Section(routine: routine))
            print("length of routine\(i)")
        }
        return sectionArray
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //taskOne = Task(name: "one", time: 123, songs: [(songs?[2])!,(songs?[3])!])
        //taskTwo = Task(name: "two", time: 234, songs: [(songs?[4])!])
        //taskThree = Task(name: "three", time: 345, songs: [(songs?[5])!])
       routineArray = loadRoutine()
        if routineArray.count == 1 { //this is to populate sampleRoutine in songs
            let sampleRoutine = routineArray[0]
            for task in sampleRoutine.tasks {
                if task.songs == nil {
                    let (sampleSongs,indexSet)=generateTask(taskTime:Double(task.time), routineIndexSet: sampleRoutine.indexSet)
                    task.songs = sampleSongs
                    task.indexSet = indexSet
                    sampleRoutine.indexSet.formUnion(indexSet)
                }
            }
        }
    
        print("hello there \(routineArray[0].tasks[0].time)")
        //taskArrayOne = [taskOne, taskTwo]
        //taskArrayTwo = [taskTwo, taskThree]
        //let routine1 = Routine(name:"r1" ,tasks: taskArrayOne)
        //let routine2 = Routine(name:"r2", tasks: taskArrayTwo)
        //routineArray = [routine1,routine2]
        //routineArray.append(routine1)
       //routineArray.append(routine2)
        //saveRoutine(routineArray: routineArray)
        //sectionOne = Section(routine: routineArray.first!)
        //sectionTwo = Section(routine: routineArray.last!)
        
        sectionArray = getSectionsArrayFromRoutine(from: routineArray)
     
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        theTableView.delegate = self
        theTableView.dataSource = self
        
        theTableView.reloadData()
        
        //saveRoutine(routineArray: [routineOne, routineTwo])

    }
    
    override func viewDidAppear(_ animated: Bool) {
        theTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionArray[section].routine.name
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.sectionArray.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.sectionArray[section].routine.tasks.count + 1
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let addCell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for:indexPath) as! buttonCell
        print(indexPath.row)
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for:indexPath) as! buttonCell
            cell.selectionStyle = .none
            cell.addTaskButton.setPath(path: indexPath)
            return cell
        }
        else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for:indexPath) as! TaskCell
        let currRoutine = self.sectionArray[indexPath.section].routine
        let currTask = currRoutine.tasks[indexPath.row - 1]
        let currTaskName = currTask.name
        cell.taskName.text = currTaskName
        cell.timeVal.text = currTask.time.secondsToString()
        return cell
        }
        //saveRoutine()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = sectionArray[section].routine.name
       // header.titleText.text =  sectionArray[section].routine.name
        header.arrowLabel.text = ">"
        header.editRButton.titleLabel?.text = "Edit"
        header.playButton.titleLabel?.text = "BUTTON"
        header.playButton.setRoutine(routine: sectionArray[section].routine)
       // header.playButton.backgroundColor = UIColor(hex: 0xFF0000, alpha: 1.0)
        let myImage = UIImage(named: "button-playblackblack.png");
        let editImage = UIImage(named:"347-200.png");
        header.editRButton.setImage(editImage, for: UIControlState.normal)
        header.editRButton.addTarget (self, action: #selector(edit(button:)), for: UIControlEvents.touchUpInside)
        header.editRButton.setPath(path:IndexPath(row:0,section:section))
        header.playButton.setImage(myImage,for: UIControlState.normal)
        header.playButton.addTarget(self, action: #selector(play(button:)), for: UIControlEvents.touchUpInside)
        
        header.playButton.titleLabel?.text = "BUTTON"
        // header.playButton.backgroundColor = UIColor(hex: 0xFF0000, alpha: 1.0)
//        let plusButton = UIImage(named: "thickplusinvert.png");
//        
//        header.addTask.setImage(plusButton,for: UIControlState.normal)
//        header.addTask.addTarget(self, action: #selector(play), for: UIControlEvents.touchUpInside)
        
        header.setCollapsed(collapsed: sectionArray[section].collapsed)
        header.section = section
        header.delegate = self
        return header
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sectionArray[indexPath.section].collapsed ? 0 : 60.0
    }
    
    func play(button: RoutineButton) {
        performSegue(withIdentifier: "PlayRoutine", sender: button)
        //print("I'M FUCKING HAVING THE TIME OF MY GODDAMN LIFE WITH DEV AND ZACHARY *&#! GREENBERG")
    }
    func edit(button: IndexPathButton   ){
     let header =    theTableView.headerView(forSection: button.indexPath.section) as! CollapsibleTableViewHeader
        let routine = sectionArray[button.indexPath.section].routine
        //header.titleLabel.isEnabled = true
        //header.titleLabel.text! = "tester"
        //print(header.titleLabel.text!)
        //print("edit button is here")
        //theTableView.reloadData()
     //  header.titleLabel.isHidden = false
        
        let renameAlert = UIAlertController(title: "Rename Routine", message: "", preferredStyle: UIAlertControllerStyle.alert)
        renameAlert.addTextField { (textField) in
            textField.text = header.titleLabel.text
        }
        renameAlert.addAction(UIAlertAction (title: "Cancel", style : .default, handler: { (action: UIAlertAction!) in
            //do nothing
        }))
        renameAlert.addAction(UIAlertAction (title: "Save", style : .default, handler: { (_) in
            let newName = renameAlert.textFields![0]
            routine.name = newName.text!
            var routineArray = self.loadRoutine()
            routineArray[button.indexPath.section] = routine
            self.saveRoutine(routineArray: routineArray)
            self.theTableView.reloadData()
            
        }))
        self.present(renameAlert, animated: true, completion: nil)

        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("this is the index path.row\(indexPath.row)")
        if(indexPath.row == 0){
            print("sending indexPath.row: \(indexPath.row)")
        }
        else{
            performSegue(withIdentifier: "EditTask", sender: self)
        }
       
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "EditTask") {
            let vc = segue.destination as! EditTaskViewController
            if let indexPath = self.theTableView.indexPathForSelectedRow {
                
                let routine = sectionArray[indexPath.section].routine
                
                vc.indexPath = indexPath
                vc.sectionArray = sectionArray
                vc.task = routine.tasks[indexPath.row - 1]
                print("we're gonna display \(String(describing: vc.task?.name)) because we got to the \(indexPath.row)th item")
            }
            
        }
        else if(segue.identifier == "newTaskSegue"){
            let caller = sender as! IndexPathButton
            let vc = segue.destination as! EditTaskViewController
            let indexPath = caller.getPath()
        
            let routine = sectionArray[indexPath.section].routine
            let (newSongs, indexSet) = generateTask(taskTime: Double(90), routineIndexSet :routine.indexSet)
            let sampleTask = Task(name: "New Task", time: 90, songs: newSongs )
            sampleTask.indexSet = indexSet
            routine.indexSet.formUnion(indexSet)
            routine.tasks.append(sampleTask)
            vc.indexPath = indexPath
            vc.task = routine.tasks.last! //this should not fail
            vc.sectionArray = sectionArray

            print("this is the indexPath\(indexPath.row)")
            
        }
        else if(segue.identifier == "PlayRoutine") {
            let vc = segue.destination as! playRoutineViewController
            let button = sender as! RoutineButton
            let routine = button.routine
            //let taskOne = Task(name: "one", time: 123, songs: [(songs?[2])!,(songs?[3])!])
            //let taskTwo = Task(name: "two", time: 200, songs: [(songs?[4])!])
            //let taskThree = Task(name:"three", time: 200, songs: [(songs?[5])!, (songs?[6])!])
            
            //let taskArrayOne = [taskOne, taskTwo]
            //let taskArrayTwo = [taskTwo, taskThree]
            //let routine = Routine(name:"r1" ,tasks: taskArrayOne)

            print("routine is: \(routine.name)")
            vc.routine = routine
            
                
        }
    }
    func runAlgo(taskTime : Double, indexSet : Set<Int>) ->
        ([MPMediaItem],Set<Int>) {
            if taskTime > 20 {
                var playlistArray : [MPMediaItem] = []
                var i = Int(arc4random_uniform(UInt32(songs!.count)))
                var j=0
                while(((songs?[i].playbackDuration)! > taskTime || indexSet.contains(i) || (taskTime>40&&(songs?[i].playbackDuration)! < 20)) && j < 500) {
                    //let time = songs?[i].playbackDuration
                    //print(" \(taskTime) < \(time!) ")
                    i = Int(arc4random_uniform(UInt32(songs!.count)))
                    j += 1
                }
                if j >= 500 {
                    return ([],indexSet)
                } else {
                    let song_to_add = songs?[i]
                    var newIndexSet = indexSet
                    newIndexSet.insert(i)
                    playlistArray.append(song_to_add!)
                    //print("hi")
                    let newTime = taskTime - (songs?[i].playbackDuration)!
                    let (other_songs,newerIndexSet) = runAlgo(taskTime: newTime,indexSet: newIndexSet)
                    for song in other_songs {
                        playlistArray.append(song)
                    }
                    return (playlistArray, newerIndexSet)
                }
            } else {
                return ([],indexSet)
            }
            
    }
    
    func calcDeadtime( newPlaylist : [MPMediaItem], taskTime: Double ) -> Double {
        var playlistDeadtime = taskTime
        for  song in newPlaylist {
            playlistDeadtime = playlistDeadtime - song.playbackDuration
        }
        
        return playlistDeadtime
        
    }
    
    func generateTask(taskTime: Double, routineIndexSet : Set<Int>) -> ([MPMediaItem],Set<Int>) {
        var bestPlaylist : [MPMediaItem] = []
        var bestDeadtime = taskTime
        var bestIndexSet = Set<Int>()
        for _ in 1...100 {
            let (newPlaylist,newIndexSet) = runAlgo(taskTime:taskTime,indexSet: routineIndexSet)
            let playlistDeadtime = calcDeadtime(newPlaylist: newPlaylist, taskTime: taskTime)
            if playlistDeadtime < bestDeadtime {
                bestPlaylist = newPlaylist
                bestDeadtime = playlistDeadtime
                bestIndexSet = newIndexSet
            }
        }
        //print(bestIndexSet)
        //print("\(bestPlaylist.count) this is the number of songs in the best playlist for this task")
        print("new deadtime: \(bestDeadtime)")
        return (bestPlaylist,bestIndexSet)
        //        print("\(playlist.count) this is the number of tasks that we have playlists built for this routine")
        //        print("\(playlist[0][0].title) is the first song")
        //        print("\(playlist[1][0].title) is the second song")
        //        print("\(playlist[2][0].title) is the song that begins the third task")
        //        print("\(playlist[2][1].title) is the second song of the third task")
        //        print("\(playlist[2][2].title) is the final song of the third task")
        //        for task in playlist {
        //            for song in task {
        //                print(song.title)
        //            }
        //        }
    }
    


}

extension UIViewController {
    
    func saveRoutine(routineArray: [Routine]) {
        let userDefaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: routineArray)
        userDefaults.set(encodedData, forKey: "routines")
//        userDefaults.synchronize()
    }
    
    func loadRoutine() -> [Routine] {
        let userDefaults = UserDefaults.standard
        let decoded = userDefaults.object(forKey: "routines") as? Data
        if (decoded == nil) {
            return [Routine(name: "Sample Routine", tasks: [Task(name: "Brushing", time: 120, songs: nil)])]
        }
        //TODO: remove forced casting
        let decodedRoutines = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [Routine]
        userDefaults.synchronize()
        return decodedRoutines
    }
    
    
}

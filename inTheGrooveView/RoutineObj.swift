//
//  RoutineObj.swift
//  inTheGrooveView
//
//  Created by zach greenberg on 4/6/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import Foundation
// array tasks
// name
// total time
// ideal time

class Routine: NSObject, NSCoding {
    var name: String
    
    var tasks: [Task]
    
    var indexSet : Set<Int> = Set<Int>()
    
    //Precomputed property: total time for all tasks
    var idealTime: Int {
        //Warning: may crash. If so, check to see if it is empty first
        
        var temp: Int = 0
        for item in tasks {
            temp += item.time
        }
        return temp
        
    }
    
    
    //Precomputed property: total dead time for all tasks
    //We're keeping this information for now, although we may never use it
    var deadTime: TimeInterval? {
        //Warning: may crash. If so, check to see if it is empty first
        
        if (tasks.isEmpty) {
            return nil
        } else if (tasks[0].deadTime == nil) {
            return nil
        }
        
        var temp: TimeInterval = 0
        for item in tasks {
            if let unwrappedTime = item.deadTime {
                temp += unwrappedTime
            }
        }
        return temp
    }
    
    init(name: String, tasks: [Task]) {
        self.name = name
        self.tasks = tasks
        for task in tasks {
            self.indexSet.formUnion(task.indexSet)
        }
    }
    
    
    //Allow for storage in UserDefaults
    
    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        let tasks = aDecoder.decodeObject(forKey: "tasks") as? [Task] ?? [Task(name: "default", time: 5, songs: nil)]
        self.init(name: name, tasks: tasks)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(tasks, forKey: "tasks")
    }
    
    
}

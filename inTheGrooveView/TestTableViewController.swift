//
//  TestTableViewController.swift
//  inTheGrooveView
//
//  Created by Rishil on 4/10/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit

class TestTableViewController: UITableViewController {
    
    var taskOne : Task!
    var taskTwo : Task!
    var taskThree : Task!
    
    var taskArrayOne : [Task]!
    var taskArrayTwo : [Task]!
    var routineOne : Routine!
    var routineTwo: Routine!
    
    var routineArray : [Routine]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        taskOne = Task(name: "one", time: 123)
        taskTwo = Task(name: "two", time: 234)
        taskThree = Task(name: "three", time: 345)
        
        taskArrayOne = [taskOne, taskTwo]
        taskArrayTwo = [taskTwo, taskThree]
        routineOne = Routine(name: "rOne", tasks: taskArrayOne, idealTime: 123)
        routineTwo = Routine(name: "rTwo", tasks: taskArrayTwo, idealTime: 234)
        
        routineArray = [routineOne, routineTwo]
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return routineArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "outerCell", for: indexPath) as! OuterCell
        
        

        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

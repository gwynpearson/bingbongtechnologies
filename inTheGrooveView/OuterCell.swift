//
//  OuterCell.swift
//  inTheGrooveView
//
//  Created by Rishil on 4/10/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit

class OuterCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var innerTableView: UITableView!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

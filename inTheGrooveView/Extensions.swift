//
//  Extensions.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 9/28/16.
//  Copyright © 2016 Yong Su. All rights reserved.
//
import UIKit

extension UIColor {
    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
}

extension UIView {
    
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        
        self.layer.add(animation, forKey: nil)
    }
    
   
}

extension SectionTestTableViewController: CollapsibleTableViewHeaderDelegate {
    func toggleSection(header: CollapsibleTableViewHeader, section: Int) {
        let collapsed = !sectionArray[section].collapsed
        //Toggle Collapse
        sectionArray[section].collapsed = collapsed
        header.setCollapsed(collapsed: collapsed)
        //Adjust hte height of the rows inside the section
        theTableView.beginUpdates()
        for i in 0 ..< sectionArray[section].routine.tasks.count {
            theTableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
        }
        
        theTableView.endUpdates()
    }
}

extension Int {
    func secondsToString() -> String {
        let hours = (Int)(self / (60*60))
        var t2 = Double(self) - (Double)(hours)*60.0*60.0
        let minutes = (Int)(t2/60);
        t2 = t2 - (Double)(minutes)*60.0
        let seconds = (Int)(t2)
        let strHours = String(format: "%02d", hours)
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        let s = "\(strHours):\(strMinutes):\(strSeconds)"
        return s
    }
}

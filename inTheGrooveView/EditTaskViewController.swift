//
//  EditTaskViewController.swift
//  inTheGrooveView
//
//  Created by Rishil on 4/13/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit
import MediaPlayer

class EditTaskViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var task: Task!
    var sectionArray: [Section]!
    var indexPath: IndexPath!
    
    let pickerData = [Array(1...30), [0,5,10,15,20,25,30,35,40,45,50,55]] //minutes, seconds. Minutes max value is hardcoded!
    // TODO: Make seconds increment by 5
    var currentMinutesAndSeconds = [1,30]
    var songs : [MPMediaItem]? = MPMediaQuery.songs().items
    
    
    @IBOutlet weak var editTitleField: UITextField!
    
    @IBOutlet weak var myPickerView: UIPickerView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
       
        editTitleField.text = task.name
        currentMinutesAndSeconds = task.getMinutesAndSeconds()

        
        
        myPickerView.dataSource = self
        myPickerView.delegate = self
        
        print("minutes: \(pickerData[0][getMinuteIndex(minutes: currentMinutesAndSeconds[0])])")
        print("second: \(pickerData[1][getSecondIndex(seconds: currentMinutesAndSeconds[1])])")
        
        myPickerView.selectRow(getMinuteIndex(minutes: currentMinutesAndSeconds[0]), inComponent: 0, animated: false)
        myPickerView.selectRow(getSecondIndex(seconds: currentMinutesAndSeconds[1]), inComponent: 1, animated: false)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
//    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
//        if(component == 0) {
//            return 55
//        } else {
//            return 40
//        }
//    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return String(pickerData[component][row])
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentMinutesAndSeconds[component] = pickerData[component][row]
    }
    
    @IBAction func save(_ sender: Any) {
        
        if(currentMinutesAndSeconds[0] == 0){
            currentMinutesAndSeconds[0] = 1
        }
        print("currentMinutes\(currentMinutesAndSeconds[0])")
        print("currentSeconds\(currentMinutesAndSeconds[1])")
        let time = 60*currentMinutesAndSeconds[0] + currentMinutesAndSeconds[1]
        task.time = time
        task.name = editTitleField.text ?? ""
        print("task time:\(task.time)")
        print("task name:\(task.name)")
        //call algorithm to get new list of songs for this task
        let routine = sectionArray[indexPath.section].routine
        for i in task.indexSet {
            routine.indexSet.remove(i)
        }
        let (songs,indexSet) = generateTask(taskTime: Double(time), routineIndexSet: routine.indexSet)
        
        print(indexSet)
        routine.indexSet.formUnion(indexSet)
        task.indexSet = indexSet
        task.songs = songs
        
        
//        routine.tasks[indexPath.row - 1] = task
//        print("task time in routine:\(routine.tasks[indexPath.row - 1].time)")
//        sectionArray[indexPath.section].routine = routine

        
        if(indexPath.row == 0) {
            //because it keeps adding an extra goddamn task at the end for no good reason!
            sectionArray[indexPath.section].routine.tasks.removeLast()
        }
        
        //var routine = sectionArray[indexPath.section].routine
        
//        if(indexPath.row == 0) {
//            routine.tasks[routine.tasks.endIndex - 1] = task
//        } else {
//            routine.tasks[indexPath.row - 1] = task
//            print("task time in routine:\(routine.tasks[indexPath.row - 1].time)")
//        }
//        
//        
//        sectionArray[indexPath.section].routine = routine
        
        //Put routine in userdefaults
        let routineArray = buildRoutineArray(from: sectionArray)
        saveRoutine(routineArray: routineArray)
        performSegue(withIdentifier: "unwindFromEdit", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "unwindFromEdit") {
            let vc = segue.destination as! SectionTestTableViewController
//            vc.routineArray = buildRoutineArray(from: sectionArray)
//            vc.sectionArray = sectionArray
        } else if(segue.identifier == "CancelTask") {
            
            if(indexPath.row == 0) {
                //because it keeps adding an extra goddamn task at the end for no good reason!
                sectionArray[indexPath.section].routine.tasks.removeLast()
                
                //this one is legitimate
                sectionArray[indexPath.section].routine.tasks.removeLast()
            }
        }

    }
    
    func buildRoutineArray(from sectionArray: [Section]) -> [Routine] {
        var array : [Routine] = []
        for section in sectionArray {
            array.append(section.routine)
        }
        return array
    }
    
    func getMinuteIndex(minutes: Int) -> Int {
        if (minutes == 0) {
            return 0
        }
        return minutes - 1 //hardcoded
    }
    
    func getSecondIndex(seconds: Int) -> Int {
        return seconds / 5 // hardcoded
    }

    @IBAction func cancel(_ sender: Any) {
    
        performSegue(withIdentifier: "CancelTask", sender: self)
    }
    
    
    func runAlgo(taskTime : Double, indexSet : Set<Int>) ->
        ([MPMediaItem],Set<Int>) {
            if taskTime > 20 {
                var playlistArray : [MPMediaItem] = []
                var i = Int(arc4random_uniform(UInt32(songs!.count)))
                var j=0
                while(((songs?[i].playbackDuration)! > taskTime || indexSet.contains(i) || (taskTime>40&&(songs?[i].playbackDuration)! < 20)) && j < 500) {
                    //let time = songs?[i].playbackDuration
                    //print(" \(taskTime) < \(time!) ")
                    i = Int(arc4random_uniform(UInt32(songs!.count)))
                    j += 1
                }
                if j >= 500 {
                    return ([],indexSet)
                } else {
                    let song_to_add = songs?[i]
                    var newIndexSet = indexSet
                    newIndexSet.insert(i)
                    playlistArray.append(song_to_add!)
                    //print("hi")
                    let newTime = taskTime - (songs?[i].playbackDuration)!
                    let (other_songs,newerIndexSet) = runAlgo(taskTime: newTime,indexSet: newIndexSet)
                    for song in other_songs {
                        playlistArray.append(song)
                    }
                    return (playlistArray, newerIndexSet)
                }
            } else {
                return ([],indexSet)
            }
            
    }
    
    func calcDeadtime( newPlaylist : [MPMediaItem], taskTime: Double ) -> Double {
        var playlistDeadtime = taskTime
        for  song in newPlaylist {
            playlistDeadtime = playlistDeadtime - song.playbackDuration
        }
        
        return playlistDeadtime
        
    }
    
    func generateTask(taskTime: Double, routineIndexSet : Set<Int>) -> ([MPMediaItem],Set<Int>) {
        var bestPlaylist : [MPMediaItem] = []
        var bestDeadtime = taskTime
        var bestIndexSet = Set<Int>()
        for _ in 1...100 {
            let (newPlaylist,newIndexSet) = runAlgo(taskTime:taskTime,indexSet: routineIndexSet)
            let playlistDeadtime = calcDeadtime(newPlaylist: newPlaylist, taskTime: taskTime)
            if playlistDeadtime < bestDeadtime {
                bestPlaylist = newPlaylist
                bestDeadtime = playlistDeadtime
                bestIndexSet = newIndexSet
            }
        }
        //print(bestIndexSet)
        print("\(bestPlaylist.count) this is the number of songs in the best playlist for this task")
        print("new deadtime: \(bestDeadtime)")
        return (bestPlaylist,bestIndexSet)
//        print("\(playlist.count) this is the number of tasks that we have playlists built for this routine")
//        print("\(playlist[0][0].title) is the first song")
//        print("\(playlist[1][0].title) is the second song")
//        print("\(playlist[2][0].title) is the song that begins the third task")
        //        print("\(playlist[2][1].title) is the second song of the third task")
        //        print("\(playlist[2][2].title) is the final song of the third task")
//        for task in playlist {
//            for song in task {
//                print(song.title)
//            }
//        }
    }
    
}

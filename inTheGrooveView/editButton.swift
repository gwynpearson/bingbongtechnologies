//
//  editButton.swift
//  inTheGrooveView
//
//  Created by zach greenberg on 4/26/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit

class editButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var routine : Routine = Routine(name:"",tasks:[])
    
    
    func getRoutine() -> Routine {
        return routine
    }
    
    func setRoutine(routine: Routine) {
        self.routine = routine
    }

}

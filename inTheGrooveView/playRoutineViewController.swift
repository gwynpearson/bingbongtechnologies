//
//  playRoutineViewController.swift
//  inTheGrooveView
//
//  Created by Gwyneth Pearson on 4/10/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import UIKit
import MediaPlayer
import Dispatch

class playRoutineViewController: UIViewController {
    
    @IBOutlet weak var routineTitle: UILabel!
    @IBOutlet weak var songTitle: UILabel!
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var countdown: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var nextTask: UIButton!
    @IBOutlet weak var endRoutine: UIButton!
    @IBOutlet weak var artistTitle: UILabel!
    var player = MPMusicPlayerController()
    var currentTaskIndex: Int = 0
    var currentTaskTime : Double = 0
    var taskLength: Int = 0
    var isPlaying : Bool = false
    var timer: Timer!
    var routine : Routine!
    var songs : [MPMediaItem]? = MPMediaQuery.songs().items


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Add shadow effects to all labels:
        
//        let routineRect = CGRect(x: routineTitle.layer.bounds.origin.x + 50, y: routineTitle.layer.bounds.origin.y + 20, width: routineTitle.layer.bounds.width - 100, height: routineTitle.layer.bounds.height - 40)
//        routineTitle.layer.shadowPath = UIBezierPath(rect: routineRect).cgPath
//        routineTitle.layer.shadowRadius = 10
//        routineTitle.layer.shadowOpacity = 0.3
//        let songRect = CGRect(x: songTitle.layer.bounds.origin.x + 20, y: songTitle.layer.bounds.origin.y + 20, width: songTitle.layer.bounds.width - 40, height: songTitle.layer.bounds.height - 40)
//        songTitle.layer.shadowPath = UIBezierPath(rect: songRect).cgPath
//        songTitle.layer.shadowRadius = 10
//        songTitle.layer.shadowOpacity = 0.3
        
//        let songsToTest = MPMediaQuery.songs().items
//        for (index, song) in songsToTest!.enumerated() {
//            print("\(song.title) is the \(index)th song")
//        }
//        
//        for i in 0...200 {
//            songs!.append(songsToTest![i])
//        }
        
       // routine = buildSampleRoutine2()
        // Do any additional setup after loading the view.
        
        routineTitle.text = routine.name
        pauseButton.setTitle("Pause Routine", for: UIControlState.normal)
        nextTask.setTitle("Next Task", for: UIControlState.normal)
        endRoutine.setTitle("End Routine", for: UIControlState.normal)
        
        player.beginGeneratingPlaybackNotifications()
        let center = NotificationCenter.default
        center.addObserver(self, selector:#selector(playRoutineViewController.updateNowPlaying), name: NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange, object: nil)
        
        playRoutine(routine:routine)
        
        
        //Then: put this in viewDidLoad and you'll be able to pass it as a parameter to buildSampleRoutine
       // let songsToTest = MPMediaQuery.songs().items
        //routine = buildSampleRoutine(songs: songsToTest!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buildSampleRoutine(songs:[MPMediaItem]) -> Routine {
        let task = Task(name: "Brushing2", time: 240, songs: [songs[800],songs[500]])
        let task2 = Task(name: "Shower2", time: 20, songs: [songs[202]])
        let task3 = Task(name: "Blah2", time:200, songs: [songs[90]])
        let routine = Routine(name: "2TESTROUTINE", tasks: [task,task2,task3])
        return routine
    }
    
    func buildSampleRoutine2() -> Routine {
        let taskTimes = [120.0,140.0,160.0]
        let song_arr = generateRoutine(taskTimes: taskTimes)
        let task = Task(name: "Brushing", time: 120, songs: song_arr[0])
        let task2 = Task(name: "Shower", time: 140, songs: song_arr[1])
        let task3 = Task(name: "Blah", time:160, songs: song_arr[2])
        let routine = Routine(name: "TESTROUTINE", tasks: [task,task2,task3])
        return routine
    }
    
    func playRoutine(routine: Routine) {
        if currentTaskIndex < routine.tasks.count {
            let current_task = routine.tasks[currentTaskIndex]
            taskLabel.text = current_task.name
            countdown.text = current_task.time.secondsToString()
            progress.setProgress(0, animated: true)
            print("we're gonna play the following task: \(current_task.name)")
            print(current_task.songs?[0].title)
            playTask(task: current_task)
        }
        else {
            self.player.stop()
            self.timer.invalidate()
            self.performSegue(withIdentifier: "unwindToMenu", sender: self)
        }
    }
    
    func playTask(task: Task) {
        currentTaskTime = Double(task.time)
        taskLength = task.time
        //print("\(task.songs)")
        let collection = MPMediaItemCollection(items: task.songs!)
        print("\(collection.items.first!.title) is our first song")
    
        player.setQueue(with: collection) //doesn't work for collection items that are late in the list of songs
        
        player.prepareToPlay()
        player.play()
    
        
        
        print("is playing: \((player.playbackState == MPMusicPlaybackState.playing))")

        
        //So, the player is non-functional, through no fault of our own, but through apple's fault. It's likely that it is setting the queue improperly (it's impossible to check) but the play function usually doesn't work. A while loop confirms that if you keep running the play function multiple times, it will play. However, one cannot exit that while loop, as the player.isPlaying variable does not update properly. Even when it does play, it plays a random song. There is no correlation between the songs we set in the queue and the songs it plays. It is important to note that none of these problems were occurring a few days ago, though the code has stayed the same.
        
        let nowPlayingItem = player.nowPlayingItem
        print("now playing: \(nowPlayingItem!.title)")
        songTitle.text = player.nowPlayingItem?.title
        artistTitle.text = player.nowPlayingItem?.artist
        startTimer(time: Double(task.time))
        isPlaying = true
    }
    

    
    
    
    func updateNowPlaying() {
        if player.nowPlayingItem != nil {
            let current_song = player.nowPlayingItem
            songTitle.text = current_song?.title
            artistTitle.text = player.nowPlayingItem?.artist
        }
        else {
            player.stop()
        }
    }
    
    func startTimer(time: Double) {
        
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(tick(timer:)), userInfo: nil, repeats: true)
        
    }
    
    func tick(timer: Timer) {
        if(currentTaskTime > 0) {
            
            //Update the global state of how much time we have left in the task
            currentTaskTime -= 0.1
            
            
            
        } else {
            //go to next task
            timer.invalidate()
            player.stop()
            currentTaskIndex += 1
            playRoutine(routine: routine)
            
        }
        //Display the remaining time
        countdown.text = secondsToString(t: currentTaskTime)
        
        //Display remaining time in progress bar
        let fractionalProgress = (Double(taskLength) - currentTaskTime)/Double(taskLength)
        progress.setProgress(Float(fractionalProgress), animated: true)
    }
    
    @IBAction func togglePlay(_ sender: Any) {
        if(isPlaying == true) {
            player.pause()
            isPlaying = false
            pauseButton.setTitle("Resume Routine", for: UIControlState.normal)
            timer.invalidate()
        } else {
            player.play()
            isPlaying = true
            pauseButton.setTitle("Pause Routine", for: UIControlState.normal)
            startTimer(time: currentTaskTime)
        }
    }
    
    @IBAction func nextTask(_ sender: Any) {
        let nextAlert = UIAlertController(title: "Next Task", message: "Are you sure you want to skip this task?", preferredStyle: UIAlertControllerStyle.alert)
        nextAlert.addAction(UIAlertAction (title: "Cancel", style : .default, handler: { (action: UIAlertAction!) in
            //do nothing
        }))
        nextAlert.addAction(UIAlertAction (title: "Ok", style : .default, handler: { (_) in
            self.currentTaskIndex += 1
            self.player.stop()
            self.timer.invalidate()
            self.pauseButton.setTitle("Pause Routine", for: UIControlState.normal)
            self.playRoutine(routine: self.routine)
            
        }))
        
        self.present(nextAlert, animated: true, completion: nil)
        
    }
    @IBAction func endRoutine(_ sender: Any) {
        let endAlert = UIAlertController(title: "End Routine", message: "All progress will be lost", preferredStyle: UIAlertControllerStyle.alert)
        endAlert.addAction(UIAlertAction (title: "Cancel", style : .default, handler: { (action: UIAlertAction!) in
            //do nothing
        }))
        endAlert.addAction(UIAlertAction (title: "Ok", style : .default, handler: { (_) in
            self.player.stop()
            self.timer.invalidate()
            self.performSegue(withIdentifier: "unwindToMenu", sender: self)
        }))
        
        self.present(endAlert, animated: true, completion: nil)
    }
    
    func secondsToString(t:Double) -> String {
        let hours = (Int)(t / (60*60))
        var t2 = Double(t) - (Double)(hours)*60.0*60.0
        let minutes = (Int)(t2/60);
        t2 = t2 - (Double)(minutes)*60.0
        let seconds = (Int)(t2)
        let strHours = String(format: "%02d", hours)
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        let s = "\(strHours):\(strMinutes):\(strSeconds)"
        return s
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func runAlgo(taskTime : Double, indexSet : Set<Int>) ->
        ([MPMediaItem],Set<Int>) {
        if taskTime > 20 {
            var playlistArray : [MPMediaItem] = []
            var i = Int(arc4random_uniform(UInt32(songs!.count)))
            var j=0
            while(((songs?[i].playbackDuration)! > taskTime || indexSet.contains(i) || (taskTime>40&&(songs?[i].playbackDuration)! < 20)) && j < 500) {
                //let time = songs?[i].playbackDuration
                //print(" \(taskTime) < \(time!) ")
                i = Int(arc4random_uniform(UInt32(songs!.count)))
                j += 1
            }
            if j >= 500 {
                return ([],indexSet)
            } else {
                let song_to_add = songs?[i]
                var newIndexSet = indexSet
                newIndexSet.insert(i)
                playlistArray.append(song_to_add!)
                    //print("hi")
                let newTime = taskTime - (songs?[i].playbackDuration)!
                let (other_songs,newerIndexSet) = runAlgo(taskTime: newTime,indexSet: newIndexSet)
                for song in other_songs {
                    playlistArray.append(song)
                }
                return (playlistArray, newerIndexSet)
            }
        } else {
            return ([],indexSet)
            }
    
    }
    
    func calcDeadtime( newPlaylist : [MPMediaItem], taskTime: Double ) -> Double {
        var playlistDeadtime = taskTime
        for  song in newPlaylist {
            playlistDeadtime = playlistDeadtime - song.playbackDuration
        }
        
        return playlistDeadtime
        
    }
    
    func generateRoutine(taskTimes: [Double]) -> [[MPMediaItem]]{
        var totalIndexSet = Set<Int>()
        var playlist : [[MPMediaItem]] = []
        for taskTime in taskTimes {
            var bestPlaylist : [MPMediaItem] = []
            var bestDeadtime = taskTime
            var bestIndexSet = Set<Int>()
            for _ in 1...100 {
                let (newPlaylist,newIndexSet) = runAlgo(taskTime:taskTime,indexSet: totalIndexSet)
                let playlistDeadtime = calcDeadtime(newPlaylist: newPlaylist, taskTime: taskTime)
                if playlistDeadtime < bestDeadtime {
                    bestPlaylist = newPlaylist
                    bestDeadtime = playlistDeadtime
                    bestIndexSet = newIndexSet
                }
            }
            print(bestIndexSet)
            print("\(bestPlaylist.count) this is the number of songs in the best playlist for this task")
            playlist.append(bestPlaylist)
            print("\(bestDeadtime)")
            totalIndexSet.formUnion(bestIndexSet)
        }
        print("\(playlist.count) this is the number of tasks that we have playlists built for this routine")
        print("\(playlist[0][0].title) is the first song")
        print("\(playlist[1][0].title) is the second song")
        print("\(playlist[2][0].title) is the song that begins the third task")
//        print("\(playlist[2][1].title) is the second song of the third task")
//        print("\(playlist[2][2].title) is the final song of the third task")
        for task in playlist {
            for song in task {
                print(song.title)
            }
        }
        return playlist
    }
}

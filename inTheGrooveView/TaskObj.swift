//
//  TaskObj.swift
//  inTheGrooveView
//
//  Created by zach greenberg on 4/6/17.
//  Copyright © 2017 zach greenberg. All rights reserved.
//

import Foundation
import MediaPlayer


class Task: NSObject, NSCoding{
 
    var name: String
    var time: Int
    
    var songs: [MPMediaItem]?
    var indexSet: Set<Int> = Set<Int>()
    
    var totalMusicLength: TimeInterval? {
        if let songsArray = songs {
            var sum : Double = 0
            for song in songsArray {
                sum += song.playbackDuration
            }
            
            return sum
            
        } else {
            return nil
        }
    }
    
    var deadTime: TimeInterval? {
        if let realLength = totalMusicLength {
            return Double(time) - realLength
        } else {
            return nil
        }
    }
    init(name:String, time: Int, songs: [MPMediaItem]?){
        self.name = name
        self.time = time
        self.songs = songs
        self.indexSet = Set<Int>()
    }
    required init(coder decoder: NSCoder){
        
        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
        print(decoder.decodeObject(forKey: "name").debugDescription)
        if(decoder.decodeInteger(forKey: "time") == 0) {
            self.time = 120
        } else {
            self.time = decoder.decodeInteger(forKey: "time")
        }
        self.songs = decoder.decodeObject(forKey: "songs") as? [MPMediaItem] ?? [MPMediaItem()]
        self.indexSet = decoder.decodeObject(forKey: "indexSet") as? Set<Int> ?? Set<Int>()
        
    }
    func encode(with aCoder: NSCoder){
        aCoder.encode(name, forKey:"name")
        aCoder.encode(time, forKey:"time")
        aCoder.encode(songs, forKey:"songs")
        aCoder.encode(indexSet,forKey:"indexSet")
    }
    
    
    func getMinutesAndSeconds() -> [Int] {
        
        let minutes = Int(self.time) / 60
        let seconds = Int(self.time) - (60*minutes)
        let array = [minutes, seconds]
        
        return array
        
    }
}
